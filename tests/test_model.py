###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Tests of the SQLAlchemy ORM definitions
#

import json
from pathlib import Path

import pytest
from sqlalchemy import create_engine, select, text
from sqlalchemy.orm import Session

from lbdtsdb.models import Analysis, Base, Dataset, Tag, TagProperty

DATA_DIR = Path(__file__).parent / "data"
BOOKKEEPING_PATHS = DATA_DIR / "bookkeeping-paths.json"


@pytest.fixture
def lhcb_tag_db():
    """ Create an in-memory DB to test the functionality """
    engine = create_engine("sqlite+pysqlite:///:memory:", echo=True, future=True)
    # engine = create_engine("sqlite+pysqlite:///test.db", echo=True, future=True)
    # Create the tables from the schema
    Base.metadata.create_all(engine)
    return engine


@pytest.fixture
def bkpaths():
    """ load bookkeeping paths  """
    with open(BOOKKEEPING_PATHS) as f:
        return json.load(f)


def test_connect(lhcb_tag_db):
    with lhcb_tag_db.connect() as conn:
        result = conn.execute(text("select 'hello world'"))
        print(result.all())


def test_dataset_create(lhcb_tag_db):
    TEST_PATH1 = "/LHCb/Collision18/Conditions"
    with Session(lhcb_tag_db) as session:
        session.add(Dataset(path=(TEST_PATH1)))
        result = session.execute(select(Dataset).filter_by(path=TEST_PATH1))
        objs = list(result.scalars())
        ds = objs[0]
        assert len(objs) == 1
        assert ds.path == TEST_PATH1
        print(f"{ds.id} {ds.path}")


def test_load_paths(lhcb_tag_db, bkpaths):

    with Session(lhcb_tag_db) as session:
        for i, p in enumerate(bkpaths):
            session.add(Dataset(path=p))
            if i >= 500:
                break

        result = session.execute(select(Dataset).filter_by(path=bkpaths[0]))
        objs = list(result.scalars())
        ds = objs[0]
        assert len(objs) == 1
        assert ds.path == bkpaths[0]
        print(f"{ds.id} {ds.path}")


def test_tags(lhcb_tag_db, bkpaths):

    with Session(lhcb_tag_db) as session:

        def getds(path):
            ds = session.query(Dataset).filter_by(path=path).one_or_none()
            if ds is None:
                ds = Dataset(path=path)
                session.add(ds)
            return ds

        origdsnames = set([bkpaths[5], bkpaths[7], bkpaths[8]])
        datasets = [getds(dsn) for dsn in origdsnames]
        session.commit()

        ana1 = Analysis(name="analysis1", wg="WG1")
        t1 = Tag(name="tag1")
        t1.analysis = ana1
        for ds in datasets:
            t1.datasets.append(ds)
        session.add(ana1)
        session.add(t1)
        session.commit()

        # Find the analysis and print it out
        mytag = session.query(Tag).filter_by(name="tag1").one()
        print(mytag.analysis.name)
        newdsnames = set([p.path for p in mytag.datasets])
        print(newdsnames)
        assert newdsnames == origdsnames
        assert mytag.analysis.name == "analysis1"


def test_tags_properties(lhcb_tag_db, bkpaths):

    with Session(lhcb_tag_db) as session:

        def getds(path):
            ds = session.query(Dataset).filter_by(path=path).one_or_none()
            if ds is None:
                ds = Dataset(path=path)
                session.add(ds)
            return ds

        origdsnames = set([bkpaths[0], bkpaths[3]])
        datasets = [getds(dsn) for dsn in origdsnames]
        session.commit()

        ana1 = Analysis(name="analysis2", wg="WG1")
        t1 = Tag(name="tag1")
        t1.analysis = ana1
        for ds in datasets:
            t1.datasets.append(ds)

        props = []
        for i in range(1, 11):
            props.append(TagProperty(f"name{i}", f"value{i}"))
        t1.properties = props
        session.add(ana1)
        session.add(t1)
        session.commit()

        # Find the analysis and print it out
        mytag = session.query(Tag).filter_by(name="tag1").one()
        print(mytag)
        newdsnames = set([p.path for p in mytag.datasets])
        print(newdsnames)
        for p in mytag.properties:
            print(p)
        # assert newdsnames == origdsnames
        assert mytag.analysis.name == "analysis2"
        assert len(mytag.properties) == 10
