###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Test of the REST interface
#
import json
from pathlib import Path

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from lbdts.main import dts, get_db
from lbdtsdb.models import Analysis, Base, Dataset, Tag, TagProperty

# SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"
SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:"
DATA_DIR = Path(__file__).parent / "data"
BOOKKEEPING_PATHS = DATA_DIR / "bookkeeping-paths.json"


def get_bkpaths():
    """ load bookkeeping paths  """
    with open(BOOKKEEPING_PATHS) as f:
        return json.load(f)


def add_test_data(engine):

    Base.metadata.create_all(engine)

    bkpaths = get_bkpaths()
    with Session(engine) as session:
        datasets = [Dataset(path=p) for p in bkpaths[:10]]
        for ds in datasets:
            session.add(ds)

        for i in range(10):
            ana1 = Analysis(name=f"analysis{i}", wg="WG1")
            for j in range(10):
                t1 = Tag(name=f"tag{j}")
                t1.analysis = ana1
                t1.datasets.append(datasets[0])
                t1.datasets.append(datasets[3])
                props = []
                for k in range(1, 11):
                    props.append(TagProperty(f"name{k}", f"value{k}"))
                t1.properties = props
                session.add(t1)
            session.add(ana1)
        session.commit()


@pytest.fixture
def test_setup():
    engine = create_engine(
        SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    )
    add_test_data(engine)
    TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    def override_get_db():
        try:
            db = TestingSessionLocal()
            yield db
        finally:
            db.close()

    dts.dependency_overrides[get_db] = override_get_db
    client = TestClient(dts)
    return (engine, client)


def test_customdb(test_setup):
    engine, client = test_setup
    with Session(engine) as session:
        # Find the analysis and print it out
        alltags = session.query(Tag).filter_by(name="tag1").all()
        assert len(alltags) == 10


def test_list_analyses(test_setup):
    engine, client = test_setup
    response = client.get("/analysis/")
    print(response)
    data = response.json()
    assert len(data) == 10


def test_list_datasets(test_setup):
    engine, client = test_setup
    response = client.get("/WG1/analysis7/tag4/datasets")
    data = response.json()
    print(data)
    assert len(data) == 2
