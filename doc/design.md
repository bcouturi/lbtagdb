
## Design for the LHCb dataset tagging system


[TOC]





## Introduction

This document presents the requirements and design of a system allowing LHCb physicists to identify the data they use in their analysis, in order to improve traceability and preservation and make their work easier, the Dataset Tagging System (DTS).


## Use cases


### Actors


<table>
  <tr>
   <td><strong>Analyst</strong>
   </td>
   <td>Physicist working on a LHCb Analysis, as part of a working group
   </td>
  </tr>
  <tr>
   <td><strong>Working group convener</strong>
   </td>
   <td>Physicist coordinating the resources and the the requirements of a specific working group
   </td>
  </tr>
  <tr>
   <td><strong>System administrator</strong>
   </td>
   <td>In charge of maintaining and running the underlying system
   </td>
  </tr>
  <tr>
   <td><strong>CI system</strong>
   </td>
   <td>System running the continuous integration and test for new analysis code
   </td>
  </tr>
  <tr>
   <td><strong>DIRAC</strong>
   </td>
   <td>System running the LHCb jobs on the LHC computing grid
   </td>
  </tr>
</table>



### Use cases descriptions


#### Analyst tags dataset for use by its analysis

An Analyst can collect the list of bookkeeping paths corresponding to the data to be processed. It is necessary to attribute the tag to an analysis for proper management of the data. This could be done from within the bookkeeping browser or using a command line tool.


#### Modify or delete tags

An Analyst or WG convener should be able to update the tags for a specific analysis, using the same methods as mentioned in the use case above.


#### Batch update of tags

An Analyst or WG convener to review all the samples available and tag them in batch to avoid slow manipulations in the bookkeeping.


#### Lookup files for a specific tag

Analyst, CI system, DIRAC should be able to get the list of files for a specific file.  \
Can the tags list and Bookkeeping paths be public?


#### List tags for a specific analysis or WG

An Analyst or WG convener should be able to list all the tags for an analysis or WG.It should also be possible to select files with given characteristics (MC/Data, Datataking year, polarity, processing pass etc)


#### Lookup new files for a given tag

This could be useful during data taking for example, whereby an analyst would ask to re-run processing on new files only and then merge the result and update the histograms.


## Entities and relationships

The diagram below presents the entities known to the system:


![alt_text](images/entities.png "entities and relationships")


The core concept is the Dataset, which matches the notion of “bookkeeping path” in the LHCb bookkeeping system.

The notion of logical file has been added to the diagram for information, but should not be part as this information is the responsibility of the bookkeeping. Furthermore, as LHCb data is split in O(1e6) files, this represents a large amount of information that should not be duplicated.

Therefore, the smallest granularity of LHCb data represented in this system is the Dataset.

The Dataset is identified by a bookkeeping path, which contains information about the data taking/generation conditions and how they were processed: they are represented by the entities ConfigName, ConfigVersion, Conditions, Processing pass, Polarity, Event Type. This information could be indexed specifically to accelerate searches if needed.

The goal of this application is to allow Analysts to tag specific Datasets as being used by a specific analysis, for a specific purpose. The main relationship is therefore between datasets and Tags, which:



*   Apply to a number of datasets, as many as required.
*   Are linked to an analysis, which must be attached to a Working Group, for Data management purposes.
*   Can have a number of properties (as text) that can be queried to make help processing the data.




## System design

The DTS should provide an easy way for Analysts to tag their datasets, and to easily get the list of files corresponding to a tag. The API should not be bound to a specific language, to make sure different analysis tools can be used.

For this reason, it was decided to implement the application as a REST service (implemented with FastAPI [https://fastapi.tiangolo.com/](https://fastapi.tiangolo.com/)  ), at least for the query aspects. The data will be stored in a RDBMS, using SQLAlchemy ([https://www.sqlalchemy.org/](https://www.sqlalchemy.org/) )for the interface.


### Database design

The DTS could make use of a graph database such as Neo4j, as was done for the LHCb Software Configuration Database. The flexibility of this database makes the development very easy, and graph queries easy to implement. Neo4j is however not supported by CERN/IT, and this would add support load to the operation team.

It is therefore better to implement this small database in a RDBMS, using the SQLAlchemy library already used for LHCbDirac.


### Link with Bookkeeping

This system shares the list of datasets with the bookkeeping, so its functionality should ideally be integrated with it. Due to the load and complexity of the LHCb bookkeeping, it is however preferable to separate the two systems, during the development phase at least. This implies that the list of available dataset has to be updated in the  DTS. This synchronization could be performed:



1. By a synchronization job that updates the list in the DTS DB on a regular basis. This has the disadvantage that for a period of time the DTS will not be aware of new datasets just created.
2. On demand, every time a user tags a dataset unknown to the DTS, a query top the bookkeeping system could be done to check whether it exists (and add it to the local cache if so). This could however create extra load on the bookkeeping system, and requires the DTS to have the appropriate credentials.

In the first prototype, a synchronization job is the simplest approach to put in place and it decorrelates the code bases of LHCbDirac and of the DTS


### Authentication and authorization

The data should be made available for read-only consumption by Analysis tools in the most portable way possible, hence the use of REST endpoints to serve it.

To update the data, the authentication and authorization infrastructure already in place in the experiment should be used. Updates to the database could therefore be performed using a LHCbDirac service with direct access to the database.


### Interface with the Bookkeeping GUI

TODO

It would be useful to manage tags directly from the book-keeping web application.


### REST Interface

The REST endpoint currently serves:

*   /analysis/

*   /&lt;WG>/&lt;AnalysisName>/tags/

*   /&lt;WG>/&lt;AnalysisName>/&lt;TagName>/datasets

TODO: Such endpoints are also being planned:

*   /&lt;WG>/&lt;AnalysisName>[/&lt;Version>]/&lt;Name>/lfns

*   /&lt;WG>/&lt;AnalysisName>[/&lt;Version>]/&lt;Name>/pfns

*   /&lt;WG>/&lt;AnalysisName>/by-tag/?polarity=MagDown&year=2016

The last URL serving data such as:

[ {"name": "dataset-name", "tags": ["polarity": "MagDown", "year": "2016"], "paths": ["root://..."]}, {"name": "dataset-name2", "tags": ["polarity": "MagDown", "year": "2016"], "paths": ["root://..."]} ]


##


## Issues and assumption


### Issues



1. What happens if a dataset is modified after it has been processed (e.g. during data taking). Is it possible for the Analyst to process the files added since a given date?
2. Does the tag DB access need to be protected or can all content be made public?


### Assumptions



1. We assume that people run over Analysis Productions only


## Glossary


<table>
  <tr>
   <td><strong>Dataset</strong>
   </td>
   <td>Set of files corresponding to one path in the bookkeeping system(e.g. /LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/90000000/RDST
   </td>
  </tr>
  <tr>
   <td><strong>ConfigName</strong>
   </td>
   <td>First component of a bookkeeping path, e.g. LHCb or MC
   </td>
  </tr>
  <tr>
   <td><strong>ConfigVersion</strong>
   </td>
   <td>Second component of the bookkeeping path, e.g. Collision18 for data or 2018 for MC
   </td>
  </tr>
  <tr>
   <td><strong>Conditions</strong>
   </td>
   <td>Third part of the booking path, e.g. “Beam6500GeV-VeloClosed-MagDown” in the example above
   </td>
  </tr>
  <tr>
   <td><strong>Tag</strong>
   </td>
   <td>Name given by an analyst to a number of Datasets, in order to group them in a coherent manner.
   </td>
  </tr>
</table>



## Notes

For a script that extracts information from the analysis production bookkeeping paths:

[https://gitlab.cern.ch/bcouturi/apinfo/-/blob/main/get_ap_output_urls.py](https://gitlab.cern.ch/bcouturi/apinfo/-/blob/main/get_ap_output_urls.py)
