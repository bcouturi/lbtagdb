# Dataset Tagging system for LHCb

This code prototypes the functionality for LHCb users to tag the files that they use in the LHCb bookkeeping system. The goal is for LHCb data analysts to more easily keep track and share which part of the LHCb data they have used, to ease their work as well as long term preservation.

The design is detailed in [here](./doc/design.md)

## Tools and libraries

  * SQLAlchemy (https://www.sqlalchemy.org/)
  * FastAPI (https://fastapi.tiangolo.com/)

To run the server locally, install *uvicorn* and run:

```
lbdts.main:dts --reload
```
