###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Pydantic schemas of the objects returned by the REST interface,
# used by FastAPI to check/serialize the results
#

# from typing import List

from pydantic import BaseModel


class Analysis(BaseModel):
    working_group: str
    name: str

    class Config:
        orm_mode = True


class Dataset(BaseModel):
    path: str

    class Config:
        orm_mode = True


class Tag(BaseModel):

    name: str

    class Config:
        orm_mode = True
