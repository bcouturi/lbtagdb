###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Implementation of the REST interface with FastAPI
#
from typing import List

from fastapi import Depends, FastAPI
from sqlalchemy.orm import Session

from lbdtsdb.models import Base
from lbdtsdb.tools import get_analyses, get_datasets, get_tags

from . import schemas
from .database import SessionLocal, engine

Base.metadata.create_all(bind=engine)

dts = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@dts.get("/analysis/", response_model=List[schemas.Analysis])
async def lists_analyses(db: Session = Depends(get_db)):
    return get_analyses(db)


@dts.get("/{wg}/{analysis}/tags", response_model=List[schemas.Tag])
async def lists_tags(wg: str, analysis: str, db: Session = Depends(get_db)):
    tags = list(get_tags(wg, analysis, db))
    return tags


@dts.get("/{wg}/{analysis}/{tag}/datasets/", response_model=List[schemas.Dataset])
async def lists_datasets(
    wg: str, analysis: str, tag: str, db: Session = Depends(get_db)
):
    ds = list(get_datasets(wg, analysis, tag, db))
    return ds
