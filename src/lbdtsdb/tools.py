###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Tools and methods used to query and update the database
#
from sqlalchemy import and_, select
from sqlalchemy.orm import Session

from .models import Analysis, Tag


def get_analyses(db: Session):
    return db.query(Analysis).all()


def get_tags(wg: str, analysis: str, db: Session):
    stmt = (
        select(Tag)
        .join(Analysis)
        .filter(and_(Analysis.name == analysis, Analysis.working_group == wg))
    )
    return db.execute(stmt).scalars().all()


def get_datasets(wg: str, analysis: str, tag: str, db: Session):
    stmt = (
        select(Tag)
        .join(Analysis)
        .filter(
            and_(
                Analysis.name == analysis, Analysis.working_group == wg, Tag.name == tag
            )
        )
    )
    tag = db.execute(stmt).scalars().one()
    return tag.datasets
