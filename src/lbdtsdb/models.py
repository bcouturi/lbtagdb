###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# SQLAlchemy ORM models for the entities managed by the application
#
from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import declarative_base, relationship

# declarative base class
Base = declarative_base()


# an example mapping using the base
class Dataset(Base):
    __tablename__ = "dataset"
    __table_args__ = (UniqueConstraint("path"),)

    id = Column(Integer, primary_key=True)
    path = Column(String)
    configName = Column(String)
    configVersion = Column(String)

    def __repr__(self):
        return f"Dataset({self.path})"


class Analysis(Base):
    __tablename__ = "analysis"
    __table_args__ = (UniqueConstraint("name", "working_group"),)

    id = Column(Integer, primary_key=True)
    name = Column(String)
    working_group = Column(String)

    def __init__(self, name: str, wg: str):
        self.name = name
        self.working_group = wg

    tags = relationship("Tag", back_populates="analysis")

    def __repr__(self):
        return f"{self.working_group}/{self.name}({self.id})"


class Tag(Base):
    __tablename__ = "tag"
    __table_args__ = (UniqueConstraint("name", "analysis_id"),)

    id = Column(Integer, primary_key=True)
    name = Column(String)
    analysis_id = Column(Integer, ForeignKey("analysis.id"))

    analysis = relationship("Analysis", back_populates="tags")
    tag_datasets = relationship(
        "TagDataset", cascade="all, delete-orphan", backref="tag"
    )
    datasets = association_proxy("tag_datasets", "dataset")
    properties = relationship("TagProperty", cascade="all, delete-orphan")

    def __repr__(self):
        return f"{self.id} Tag:{self.name} Analysis:{self.analysis} nb_datasets:{len(self.datasets)}"


class TagDataset(Base):
    __tablename__ = "tag_dataset"

    dataset_id = Column(Integer, ForeignKey("dataset.id"), primary_key=True)
    tag_id = Column(Integer, ForeignKey("tag.id"), primary_key=True)

    def __init__(self, dataset):
        self.dataset = dataset

    dataset = relationship(Dataset, lazy="joined")


class TagProperty(Base):
    __tablename__ = "tagproperty"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    value = Column(String)
    tag_id = Column(Integer, ForeignKey("tag.id"))

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __repr__(self):
        return f"{self.id} {self.name}:{self.value}"
